import request from '@/utils/request'

// 查询商品主图片列表
export function listGoodsMainImg(query) {
  return request({
    url: '/tpatmoc/goodsMainImg/list',
    method: 'get',
    params: query
  })
}

// 查询商品主图片详细
export function getGoodsMainImg(id) {
  return request({
    url: '/tpatmoc/goodsMainImg/' + id,
    method: 'get'
  })
}

// 新增商品主图片
export function addGoodsMainImg(data) {
  return request({
    url: '/tpatmoc/goodsMainImg',
    method: 'post',
    data: data
  })
}

// 修改商品主图片
export function updateGoodsMainImg(data) {
  return request({
    url: '/tpatmoc/goodsMainImg',
    method: 'put',
    data: data
  })
}

// 删除商品主图片
export function delGoodsMainImg(id) {
  return request({
    url: '/tpatmoc/goodsMainImg/' + id,
    method: 'delete'
  })
}
