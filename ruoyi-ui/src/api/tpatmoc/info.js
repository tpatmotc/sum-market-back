import request from '@/utils/request'

// 查询收件地址列表
export function listInfo(query) {
  return request({
    url: '/tpatmoc/info/list',
    method: 'get',
    params: query
  })
}

// 查询收件地址详细
export function getInfo(id) {
  return request({
    url: '/tpatmoc/info/' + id,
    method: 'get'
  })
}

// 新增收件地址
export function addInfo(data) {
  return request({
    url: '/tpatmoc/info',
    method: 'post',
    data: data
  })
}

// 修改收件地址
export function updateInfo(data) {
  return request({
    url: '/tpatmoc/info',
    method: 'put',
    data: data
  })
}

// 删除收件地址
export function delInfo(id) {
  return request({
    url: '/tpatmoc/info/' + id,
    method: 'delete'
  })
}
