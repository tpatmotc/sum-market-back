import request from '@/utils/request'

// 查询管理员列表
export function listSumAdmin(query) {
  return request({
    url: '/tpatmoc/sumAdmin/list',
    method: 'get',
    params: query
  })
}

// 查询管理员详细
export function getSumAdmin(id) {
  return request({
    url: '/tpatmoc/sumAdmin/' + id,
    method: 'get'
  })
}

// 新增管理员
export function addSumAdmin(data) {
  return request({
    url: '/tpatmoc/sumAdmin',
    method: 'post',
    data: data
  })
}

// 修改管理员
export function updateSumAdmin(data) {
  return request({
    url: '/tpatmoc/sumAdmin',
    method: 'put',
    data: data
  })
}

// 删除管理员
export function delSumAdmin(id) {
  return request({
    url: '/tpatmoc/sumAdmin/' + id,
    method: 'delete'
  })
}
