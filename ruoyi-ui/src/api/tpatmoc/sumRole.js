import request from '@/utils/request'

// 查询角色列表
export function listSumRole(query) {
  return request({
    url: '/tpatmoc/sumRole/list',
    method: 'get',
    params: query
  })
}

// 查询角色详细
export function getSumRole(id) {
  return request({
    url: '/tpatmoc/sumRole/' + id,
    method: 'get'
  })
}

// 新增角色
export function addSumRole(data) {
  return request({
    url: '/tpatmoc/sumRole',
    method: 'post',
    data: data
  })
}

// 修改角色
export function updateSumRole(data) {
  return request({
    url: '/tpatmoc/sumRole',
    method: 'put',
    data: data
  })
}

// 删除角色
export function delSumRole(id) {
  return request({
    url: '/tpatmoc/sumRole/' + id,
    method: 'delete'
  })
}
