import request from '@/utils/request'

// 查询商品详细列表
export function listGoodsDetail(query) {
  return request({
    url: '/tpatmoc/goodsDetail/list',
    method: 'get',
    params: query
  })
}

// 查询商品详细详细
export function getGoodsDetail(skuid) {
  return request({
    url: '/tpatmoc/goodsDetail/' + skuid,
    method: 'get'
  })
}

// 新增商品详细
export function addGoodsDetail(data) {
  return request({
    url: '/tpatmoc/goodsDetail',
    method: 'post',
    data: data
  })
}

// 修改商品详细
export function updateGoodsDetail(data) {
  return request({
    url: '/tpatmoc/goodsDetail',
    method: 'put',
    data: data
  })
}

// 删除商品详细
export function delGoodsDetail(skuid) {
  return request({
    url: '/tpatmoc/goodsDetail/' + skuid,
    method: 'delete'
  })
}
