import request from '@/utils/request'

// 查询商品介绍图片列表
export function listGoodsIntroImg(query) {
  return request({
    url: '/tpatmoc/goodsIntroImg/list',
    method: 'get',
    params: query
  })
}

// 查询商品介绍图片详细
export function getGoodsIntroImg(id) {
  return request({
    url: '/tpatmoc/goodsIntroImg/' + id,
    method: 'get'
  })
}

// 新增商品介绍图片
export function addGoodsIntroImg(data) {
  return request({
    url: '/tpatmoc/goodsIntroImg',
    method: 'post',
    data: data
  })
}

// 修改商品介绍图片
export function updateGoodsIntroImg(data) {
  return request({
    url: '/tpatmoc/goodsIntroImg',
    method: 'put',
    data: data
  })
}

// 删除商品介绍图片
export function delGoodsIntroImg(id) {
  return request({
    url: '/tpatmoc/goodsIntroImg/' + id,
    method: 'delete'
  })
}
