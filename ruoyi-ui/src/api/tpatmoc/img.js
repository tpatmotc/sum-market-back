import request from '@/utils/request'

// 查询评论图片列表
export function listImg(query) {
  return request({
    url: '/tpatmoc/img/list',
    method: 'get',
    params: query
  })
}

// 查询评论图片详细
export function getImg(id) {
  return request({
    url: '/tpatmoc/img/' + id,
    method: 'get'
  })
}

// 新增评论图片
export function addImg(data) {
  return request({
    url: '/tpatmoc/img',
    method: 'post',
    data: data
  })
}

// 修改评论图片
export function updateImg(data) {
  return request({
    url: '/tpatmoc/img',
    method: 'put',
    data: data
  })
}

// 删除评论图片
export function delImg(id) {
  return request({
    url: '/tpatmoc/img/' + id,
    method: 'delete'
  })
}
