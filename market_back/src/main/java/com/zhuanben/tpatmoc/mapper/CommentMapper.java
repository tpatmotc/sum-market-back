package com.zhuanben.tpatmoc.mapper;

import java.util.List;
import com.zhuanben.tpatmoc.domain.Comment;
import com.zhuanben.tpatmoc.domain.CommentImg;

/**
 * 用户评论Mapper接口
 * 
 * @author ldp
 * @date 2022-11-10
 */
public interface CommentMapper 
{
    /**
     * 查询用户评论
     * 
     * @param id 用户评论主键
     * @return 用户评论
     */
    public Comment selectCommentById(Long id);

    /**
     * 查询用户评论列表
     * 
     * @param comment 用户评论
     * @return 用户评论集合
     */
    public List<Comment> selectCommentList(Comment comment);

    /**
     * 新增用户评论
     * 
     * @param comment 用户评论
     * @return 结果
     */
    public int insertComment(Comment comment);

    /**
     * 修改用户评论
     * 
     * @param comment 用户评论
     * @return 结果
     */
    public int updateComment(Comment comment);

    /**
     * 删除用户评论
     * 
     * @param id 用户评论主键
     * @return 结果
     */
    public int deleteCommentById(Long id);

    /**
     * 批量删除用户评论
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCommentByIds(Long[] ids);

    /**
     * 批量删除评论图片
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCommentImgByCommIds(Long[] ids);
    
    /**
     * 批量新增评论图片
     * 
     * @param commentImgList 评论图片列表
     * @return 结果
     */
    public int batchCommentImg(List<CommentImg> commentImgList);
    

    /**
     * 通过用户评论主键删除评论图片信息
     * 
     * @param id 用户评论ID
     * @return 结果
     */
    public int deleteCommentImgByCommId(Long id);
}
