package com.zhuanben.tpatmoc.mapper;

import java.util.List;
import com.zhuanben.tpatmoc.domain.Admin;

/**
 * 管理员Mapper接口
 * 
 * @author ldp
 * @date 2022-11-11
 */
public interface AdminMapper 
{
    /**
     * 查询管理员
     * 
     * @param id 管理员主键
     * @return 管理员
     */
    public Admin selectAdminById(Long id);

    /**
     * 查询管理员列表
     * 
     * @param admin 管理员
     * @return 管理员集合
     */
    public List<Admin> selectAdminList(Admin admin);

    /**
     * 新增管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    public int insertAdmin(Admin admin);

    /**
     * 修改管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    public int updateAdmin(Admin admin);

    /**
     * 删除管理员
     * 
     * @param id 管理员主键
     * @return 结果
     */
    public int deleteAdminById(Long id);

    /**
     * 批量删除管理员
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAdminByIds(Long[] ids);
}
