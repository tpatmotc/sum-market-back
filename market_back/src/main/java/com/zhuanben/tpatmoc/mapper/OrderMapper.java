package com.zhuanben.tpatmoc.mapper;

import java.util.List;
import com.zhuanben.tpatmoc.domain.Order;
import com.zhuanben.tpatmoc.domain.OrderDetail;

/**
 * 订单Mapper接口
 * 
 * @author ldp
 * @date 2022-11-11
 */
public interface OrderMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public Order selectOrderById(Long id);

    /**
     * 查询订单列表
     * 
     * @param order 订单
     * @return 订单集合
     */
    public List<Order> selectOrderList(Order order);

    /**
     * 新增订单
     * 
     * @param order 订单
     * @return 结果
     */
    public int insertOrder(Order order);

    /**
     * 修改订单
     * 
     * @param order 订单
     * @return 结果
     */
    public int updateOrder(Order order);

    /**
     * 删除订单
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteOrderById(Long id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderByIds(Long[] ids);

    /**
     * 批量删除订单详情
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderDetailByOids(Long[] ids);
    
    /**
     * 批量新增订单详情
     * 
     * @param orderDetailList 订单详情列表
     * @return 结果
     */
    public int batchOrderDetail(List<OrderDetail> orderDetailList);
    

    /**
     * 通过订单主键删除订单详情信息
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteOrderDetailByOid(Long id);
}
