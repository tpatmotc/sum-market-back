package com.zhuanben.tpatmoc.mapper;

import java.util.List;
import com.zhuanben.tpatmoc.domain.GoodIntroImg;

/**
 * 商品介绍图片Mapper接口
 * 
 * @author ldp
 * @date 2022-11-11
 */
public interface GoodIntroImgMapper 
{
    /**
     * 查询商品介绍图片
     * 
     * @param id 商品介绍图片主键
     * @return 商品介绍图片
     */
    public GoodIntroImg selectGoodIntroImgById(Long id);

    /**
     * 查询商品介绍图片列表
     * 
     * @param goodIntroImg 商品介绍图片
     * @return 商品介绍图片集合
     */
    public List<GoodIntroImg> selectGoodIntroImgList(GoodIntroImg goodIntroImg);

    /**
     * 新增商品介绍图片
     * 
     * @param goodIntroImg 商品介绍图片
     * @return 结果
     */
    public int insertGoodIntroImg(GoodIntroImg goodIntroImg);

    /**
     * 修改商品介绍图片
     * 
     * @param goodIntroImg 商品介绍图片
     * @return 结果
     */
    public int updateGoodIntroImg(GoodIntroImg goodIntroImg);

    /**
     * 删除商品介绍图片
     * 
     * @param id 商品介绍图片主键
     * @return 结果
     */
    public int deleteGoodIntroImgById(Long id);

    /**
     * 批量删除商品介绍图片
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGoodIntroImgByIds(Long[] ids);
}
