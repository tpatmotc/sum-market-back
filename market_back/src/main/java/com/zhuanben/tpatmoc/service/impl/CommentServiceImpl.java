package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.zhuanben.tpatmoc.domain.CommentImg;
import com.zhuanben.tpatmoc.mapper.CommentMapper;
import com.zhuanben.tpatmoc.domain.Comment;
import com.zhuanben.tpatmoc.service.ICommentService;

/**
 * 用户评论Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-10
 */
@Service
public class CommentServiceImpl implements ICommentService 
{
    @Autowired
    private CommentMapper commentMapper;

    /**
     * 查询用户评论
     * 
     * @param id 用户评论主键
     * @return 用户评论
     */
    @Override
    public Comment selectCommentById(Long id)
    {
        return commentMapper.selectCommentById(id);
    }

    /**
     * 查询用户评论列表
     * 
     * @param comment 用户评论
     * @return 用户评论
     */
    @Override
    public List<Comment> selectCommentList(Comment comment)
    {
        return commentMapper.selectCommentList(comment);
    }

    /**
     * 新增用户评论
     * 
     * @param comment 用户评论
     * @return 结果
     */
    @Transactional
    @Override
    public int insertComment(Comment comment)
    {
        int rows = commentMapper.insertComment(comment);
        insertCommentImg(comment);
        return rows;
    }

    /**
     * 修改用户评论
     * 
     * @param comment 用户评论
     * @return 结果
     */
    @Transactional
    @Override
    public int updateComment(Comment comment)
    {
        commentMapper.deleteCommentImgByCommId(comment.getId());
        insertCommentImg(comment);
        return commentMapper.updateComment(comment);
    }

    /**
     * 批量删除用户评论
     * 
     * @param ids 需要删除的用户评论主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteCommentByIds(Long[] ids)
    {
        commentMapper.deleteCommentImgByCommIds(ids);
        return commentMapper.deleteCommentByIds(ids);
    }

    /**
     * 删除用户评论信息
     * 
     * @param id 用户评论主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteCommentById(Long id)
    {
        commentMapper.deleteCommentImgByCommId(id);
        return commentMapper.deleteCommentById(id);
    }

    /**
     * 新增评论图片信息
     * 
     * @param comment 用户评论对象
     */
    public void insertCommentImg(Comment comment)
    {
        List<CommentImg> commentImgList = comment.getCommentImgList();
        Long id = comment.getId();
        if (StringUtils.isNotNull(commentImgList))
        {
            List<CommentImg> list = new ArrayList<CommentImg>();
            for (CommentImg commentImg : commentImgList)
            {
                commentImg.setCommId(id);
                list.add(commentImg);
            }
            if (list.size() > 0)
            {
                commentMapper.batchCommentImg(list);
            }
        }
    }
}
