package com.zhuanben.tpatmoc.service;

import java.util.List;
import com.zhuanben.tpatmoc.domain.CommentImg;

/**
 * 评论图片Service接口
 * 
 * @author ldp
 * @date 2022-11-10
 */
public interface ICommentImgService 
{
    /**
     * 查询评论图片
     * 
     * @param id 评论图片主键
     * @return 评论图片
     */
    public CommentImg selectCommentImgById(Long id);

    /**
     * 查询评论图片列表
     * 
     * @param commentImg 评论图片
     * @return 评论图片集合
     */
    public List<CommentImg> selectCommentImgList(CommentImg commentImg);

    /**
     * 新增评论图片
     * 
     * @param commentImg 评论图片
     * @return 结果
     */
    public int insertCommentImg(CommentImg commentImg);

    /**
     * 修改评论图片
     * 
     * @param commentImg 评论图片
     * @return 结果
     */
    public int updateCommentImg(CommentImg commentImg);

    /**
     * 批量删除评论图片
     * 
     * @param ids 需要删除的评论图片主键集合
     * @return 结果
     */
    public int deleteCommentImgByIds(Long[] ids);

    /**
     * 删除评论图片信息
     * 
     * @param id 评论图片主键
     * @return 结果
     */
    public int deleteCommentImgById(Long id);
}
