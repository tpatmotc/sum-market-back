package com.zhuanben.tpatmoc.service;

import java.util.List;
import com.zhuanben.tpatmoc.domain.GoodMainImg;

/**
 * 商品主图片Service接口
 * 
 * @author ldp
 * @date 2022-11-11
 */
public interface IGoodMainImgService 
{
    /**
     * 查询商品主图片
     * 
     * @param id 商品主图片主键
     * @return 商品主图片
     */
    public GoodMainImg selectGoodMainImgById(Long id);

    /**
     * 查询商品主图片列表
     * 
     * @param goodMainImg 商品主图片
     * @return 商品主图片集合
     */
    public List<GoodMainImg> selectGoodMainImgList(GoodMainImg goodMainImg);

    /**
     * 新增商品主图片
     * 
     * @param goodMainImg 商品主图片
     * @return 结果
     */
    public int insertGoodMainImg(GoodMainImg goodMainImg);

    /**
     * 修改商品主图片
     * 
     * @param goodMainImg 商品主图片
     * @return 结果
     */
    public int updateGoodMainImg(GoodMainImg goodMainImg);

    /**
     * 批量删除商品主图片
     * 
     * @param ids 需要删除的商品主图片主键集合
     * @return 结果
     */
    public int deleteGoodMainImgByIds(Long[] ids);

    /**
     * 删除商品主图片信息
     * 
     * @param id 商品主图片主键
     * @return 结果
     */
    public int deleteGoodMainImgById(Long id);
}
