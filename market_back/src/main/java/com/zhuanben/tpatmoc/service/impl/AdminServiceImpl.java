package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zhuanben.tpatmoc.mapper.AdminMapper;
import com.zhuanben.tpatmoc.domain.Admin;
import com.zhuanben.tpatmoc.service.IAdminService;

/**
 * 管理员Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-11
 */
@Service
public class AdminServiceImpl implements IAdminService 
{
    @Autowired
    private AdminMapper adminMapper;

    /**
     * 查询管理员
     * 
     * @param id 管理员主键
     * @return 管理员
     */
    @Override
    public Admin selectAdminById(Long id)
    {
        return adminMapper.selectAdminById(id);
    }

    /**
     * 查询管理员列表
     * 
     * @param admin 管理员
     * @return 管理员
     */
    @Override
    public List<Admin> selectAdminList(Admin admin)
    {
        return adminMapper.selectAdminList(admin);
    }

    /**
     * 新增管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    @Override
    public int insertAdmin(Admin admin)
    {
        return adminMapper.insertAdmin(admin);
    }

    /**
     * 修改管理员
     * 
     * @param admin 管理员
     * @return 结果
     */
    @Override
    public int updateAdmin(Admin admin)
    {
        return adminMapper.updateAdmin(admin);
    }

    /**
     * 批量删除管理员
     * 
     * @param ids 需要删除的管理员主键
     * @return 结果
     */
    @Override
    public int deleteAdminByIds(Long[] ids)
    {
        return adminMapper.deleteAdminByIds(ids);
    }

    /**
     * 删除管理员信息
     * 
     * @param id 管理员主键
     * @return 结果
     */
    @Override
    public int deleteAdminById(Long id)
    {
        return adminMapper.deleteAdminById(id);
    }
}
