package com.zhuanben.tpatmoc.service;

import java.util.List;
import com.zhuanben.tpatmoc.domain.ReceiveInfo;

/**
 * 收件地址Service接口
 * 
 * @author ldp
 * @date 2022-11-11
 */
public interface IReceiveInfoService 
{
    /**
     * 查询收件地址
     * 
     * @param id 收件地址主键
     * @return 收件地址
     */
    public ReceiveInfo selectReceiveInfoById(Long id);

    /**
     * 查询收件地址列表
     * 
     * @param receiveInfo 收件地址
     * @return 收件地址集合
     */
    public List<ReceiveInfo> selectReceiveInfoList(ReceiveInfo receiveInfo);

    /**
     * 新增收件地址
     * 
     * @param receiveInfo 收件地址
     * @return 结果
     */
    public int insertReceiveInfo(ReceiveInfo receiveInfo);

    /**
     * 修改收件地址
     * 
     * @param receiveInfo 收件地址
     * @return 结果
     */
    public int updateReceiveInfo(ReceiveInfo receiveInfo);

    /**
     * 批量删除收件地址
     * 
     * @param ids 需要删除的收件地址主键集合
     * @return 结果
     */
    public int deleteReceiveInfoByIds(Long[] ids);

    /**
     * 删除收件地址信息
     * 
     * @param id 收件地址主键
     * @return 结果
     */
    public int deleteReceiveInfoById(Long id);
}
