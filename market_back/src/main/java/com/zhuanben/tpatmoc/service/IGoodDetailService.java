package com.zhuanben.tpatmoc.service;

import java.util.List;
import com.zhuanben.tpatmoc.domain.GoodDetail;

/**
 * 商品详细Service接口
 * 
 * @author ldp
 * @date 2022-11-11
 */
public interface IGoodDetailService 
{
    /**
     * 查询商品详细
     * 
     * @param skuid 商品详细主键
     * @return 商品详细
     */
    public GoodDetail selectGoodDetailBySkuid(Long skuid);

    /**
     * 查询商品详细列表
     * 
     * @param goodDetail 商品详细
     * @return 商品详细集合
     */
    public List<GoodDetail> selectGoodDetailList(GoodDetail goodDetail);

    /**
     * 新增商品详细
     * 
     * @param goodDetail 商品详细
     * @return 结果
     */
    public int insertGoodDetail(GoodDetail goodDetail);

    /**
     * 修改商品详细
     * 
     * @param goodDetail 商品详细
     * @return 结果
     */
    public int updateGoodDetail(GoodDetail goodDetail);

    /**
     * 批量删除商品详细
     * 
     * @param skuids 需要删除的商品详细主键集合
     * @return 结果
     */
    public int deleteGoodDetailBySkuids(Long[] skuids);

    /**
     * 删除商品详细信息
     * 
     * @param skuid 商品详细主键
     * @return 结果
     */
    public int deleteGoodDetailBySkuid(Long skuid);
}
