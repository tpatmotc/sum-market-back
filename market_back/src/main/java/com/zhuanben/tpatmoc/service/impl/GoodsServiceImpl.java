package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.mapper.GoodsMapper;
import com.zhuanben.tpatmoc.domain.Goods;
import com.zhuanben.tpatmoc.service.IGoodsService;

/**
 * 商品Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-11
 */
@Service
public class GoodsServiceImpl implements IGoodsService 
{
    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 查询商品
     * 
     * @param id 商品主键
     * @return 商品
     */
    @Override
    public Goods selectGoodsById(Long id)
    {
        return goodsMapper.selectGoodsById(id);
    }

    /**
     * 查询商品列表
     * 
     * @param goods 商品
     * @return 商品
     */
    @Override
    public List<Goods> selectGoodsList(Goods goods)
    {
        return goodsMapper.selectGoodsList(goods);
    }

    /**
     * 新增商品
     * 
     * @param goods 商品
     * @return 结果
     */
    @Transactional
    @Override
    public int insertGoods(Goods goods)
    {
        int rows = goodsMapper.insertGoods(goods);
        insertGoodDetail(goods);
        return rows;
    }

    /**
     * 修改商品
     * 
     * @param goods 商品
     * @return 结果
     */
    @Transactional
    @Override
    public int updateGoods(Goods goods)
    {
        goodsMapper.deleteGoodDetailByGoodId(goods.getId());
        insertGoodDetail(goods);
        return goodsMapper.updateGoods(goods);
    }

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的商品主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteGoodsByIds(Long[] ids)
    {
        goodsMapper.deleteGoodDetailByGoodIds(ids);
        return goodsMapper.deleteGoodsByIds(ids);
    }

    /**
     * 删除商品信息
     * 
     * @param id 商品主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteGoodsById(Long id)
    {
        goodsMapper.deleteGoodDetailByGoodId(id);
        return goodsMapper.deleteGoodsById(id);
    }

    /**
     * 新增商品详细信息
     * 
     * @param goods 商品对象
     */
    public void insertGoodDetail(Goods goods)
    {
        List<GoodDetail> goodDetailList = goods.getGoodDetailList();
        Long id = goods.getId();
        if (StringUtils.isNotNull(goodDetailList))
        {
            List<GoodDetail> list = new ArrayList<GoodDetail>();
            for (GoodDetail goodDetail : goodDetailList)
            {
                goodDetail.setGoodId(id);
                list.add(goodDetail);
            }
            if (list.size() > 0)
            {
                goodsMapper.batchGoodDetail(list);
            }
        }
    }
}
