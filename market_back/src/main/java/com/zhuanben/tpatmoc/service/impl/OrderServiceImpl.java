package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.zhuanben.tpatmoc.domain.OrderDetail;
import com.zhuanben.tpatmoc.mapper.OrderMapper;
import com.zhuanben.tpatmoc.domain.Order;
import com.zhuanben.tpatmoc.service.IOrderService;

/**
 * 订单Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-11
 */
@Service
public class OrderServiceImpl implements IOrderService 
{
    @Autowired
    private OrderMapper orderMapper;

    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    @Override
    public Order selectOrderById(Long id)
    {
        return orderMapper.selectOrderById(id);
    }

    /**
     * 查询订单列表
     * 
     * @param order 订单
     * @return 订单
     */
    @Override
    public List<Order> selectOrderList(Order order)
    {
        return orderMapper.selectOrderList(order);
    }

    /**
     * 新增订单
     * 
     * @param order 订单
     * @return 结果
     */
    @Transactional
    @Override
    public int insertOrder(Order order)
    {
        order.setCreateTime(DateUtils.getNowDate());
        int rows = orderMapper.insertOrder(order);
        insertOrderDetail(order);
        return rows;
    }

    /**
     * 修改订单
     * 
     * @param order 订单
     * @return 结果
     */
    @Transactional
    @Override
    public int updateOrder(Order order)
    {
        orderMapper.deleteOrderDetailByOid(order.getId());
        insertOrderDetail(order);
        return orderMapper.updateOrder(order);
    }

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteOrderByIds(Long[] ids)
    {
        orderMapper.deleteOrderDetailByOids(ids);
        return orderMapper.deleteOrderByIds(ids);
    }

    /**
     * 删除订单信息
     * 
     * @param id 订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteOrderById(Long id)
    {
        orderMapper.deleteOrderDetailByOid(id);
        return orderMapper.deleteOrderById(id);
    }

    /**
     * 新增订单详情信息
     * 
     * @param order 订单对象
     */
    public void insertOrderDetail(Order order)
    {
        List<OrderDetail> orderDetailList = order.getOrderDetailList();
        Long id = order.getId();
        if (StringUtils.isNotNull(orderDetailList))
        {
            List<OrderDetail> list = new ArrayList<OrderDetail>();
            for (OrderDetail orderDetail : orderDetailList)
            {
                orderDetail.setOid(id);
                list.add(orderDetail);
            }
            if (list.size() > 0)
            {
                orderMapper.batchOrderDetail(list);
            }
        }
    }
}
