package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zhuanben.tpatmoc.mapper.ReceiveInfoMapper;
import com.zhuanben.tpatmoc.domain.ReceiveInfo;
import com.zhuanben.tpatmoc.service.IReceiveInfoService;

/**
 * 收件地址Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-11
 */
@Service
public class ReceiveInfoServiceImpl implements IReceiveInfoService 
{
    @Autowired
    private ReceiveInfoMapper receiveInfoMapper;

    /**
     * 查询收件地址
     * 
     * @param id 收件地址主键
     * @return 收件地址
     */
    @Override
    public ReceiveInfo selectReceiveInfoById(Long id)
    {
        return receiveInfoMapper.selectReceiveInfoById(id);
    }

    /**
     * 查询收件地址列表
     * 
     * @param receiveInfo 收件地址
     * @return 收件地址
     */
    @Override
    public List<ReceiveInfo> selectReceiveInfoList(ReceiveInfo receiveInfo)
    {
        return receiveInfoMapper.selectReceiveInfoList(receiveInfo);
    }

    /**
     * 新增收件地址
     * 
     * @param receiveInfo 收件地址
     * @return 结果
     */
    @Override
    public int insertReceiveInfo(ReceiveInfo receiveInfo)
    {
        return receiveInfoMapper.insertReceiveInfo(receiveInfo);
    }

    /**
     * 修改收件地址
     * 
     * @param receiveInfo 收件地址
     * @return 结果
     */
    @Override
    public int updateReceiveInfo(ReceiveInfo receiveInfo)
    {
        return receiveInfoMapper.updateReceiveInfo(receiveInfo);
    }

    /**
     * 批量删除收件地址
     * 
     * @param ids 需要删除的收件地址主键
     * @return 结果
     */
    @Override
    public int deleteReceiveInfoByIds(Long[] ids)
    {
        return receiveInfoMapper.deleteReceiveInfoByIds(ids);
    }

    /**
     * 删除收件地址信息
     * 
     * @param id 收件地址主键
     * @return 结果
     */
    @Override
    public int deleteReceiveInfoById(Long id)
    {
        return receiveInfoMapper.deleteReceiveInfoById(id);
    }
}
