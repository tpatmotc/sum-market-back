package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zhuanben.tpatmoc.mapper.GoodDetailMapper;
import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.service.IGoodDetailService;

/**
 * 商品详细Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-11
 */
@Service
public class GoodDetailServiceImpl implements IGoodDetailService 
{
    @Autowired
    private GoodDetailMapper goodDetailMapper;

    /**
     * 查询商品详细
     * 
     * @param skuid 商品详细主键
     * @return 商品详细
     */
    @Override
    public GoodDetail selectGoodDetailBySkuid(Long skuid)
    {
        return goodDetailMapper.selectGoodDetailBySkuid(skuid);
    }

    /**
     * 查询商品详细列表
     * 
     * @param goodDetail 商品详细
     * @return 商品详细
     */
    @Override
    public List<GoodDetail> selectGoodDetailList(GoodDetail goodDetail)
    {
        return goodDetailMapper.selectGoodDetailList(goodDetail);
    }

    /**
     * 新增商品详细
     * 
     * @param goodDetail 商品详细
     * @return 结果
     */
    @Override
    public int insertGoodDetail(GoodDetail goodDetail)
    {
        return goodDetailMapper.insertGoodDetail(goodDetail);
    }

    /**
     * 修改商品详细
     * 
     * @param goodDetail 商品详细
     * @return 结果
     */
    @Override
    public int updateGoodDetail(GoodDetail goodDetail)
    {
        return goodDetailMapper.updateGoodDetail(goodDetail);
    }

    /**
     * 批量删除商品详细
     * 
     * @param skuids 需要删除的商品详细主键
     * @return 结果
     */
    @Override
    public int deleteGoodDetailBySkuids(Long[] skuids)
    {
        return goodDetailMapper.deleteGoodDetailBySkuids(skuids);
    }

    /**
     * 删除商品详细信息
     * 
     * @param skuid 商品详细主键
     * @return 结果
     */
    @Override
    public int deleteGoodDetailBySkuid(Long skuid)
    {
        return goodDetailMapper.deleteGoodDetailBySkuid(skuid);
    }
}
