package com.zhuanben.tpatmoc.service;

import java.util.List;
import com.zhuanben.tpatmoc.domain.Comment;

/**
 * 用户评论Service接口
 * 
 * @author ldp
 * @date 2022-11-10
 */
public interface ICommentService 
{
    /**
     * 查询用户评论
     * 
     * @param id 用户评论主键
     * @return 用户评论
     */
    public Comment selectCommentById(Long id);

    /**
     * 查询用户评论列表
     * 
     * @param comment 用户评论
     * @return 用户评论集合
     */
    public List<Comment> selectCommentList(Comment comment);

    /**
     * 新增用户评论
     * 
     * @param comment 用户评论
     * @return 结果
     */
    public int insertComment(Comment comment);

    /**
     * 修改用户评论
     * 
     * @param comment 用户评论
     * @return 结果
     */
    public int updateComment(Comment comment);

    /**
     * 批量删除用户评论
     * 
     * @param ids 需要删除的用户评论主键集合
     * @return 结果
     */
    public int deleteCommentByIds(Long[] ids);

    /**
     * 删除用户评论信息
     * 
     * @param id 用户评论主键
     * @return 结果
     */
    public int deleteCommentById(Long id);
}
