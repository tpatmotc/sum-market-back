package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zhuanben.tpatmoc.mapper.GoodMainImgMapper;
import com.zhuanben.tpatmoc.domain.GoodMainImg;
import com.zhuanben.tpatmoc.service.IGoodMainImgService;

/**
 * 商品主图片Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-11
 */
@Service
public class GoodMainImgServiceImpl implements IGoodMainImgService 
{
    @Autowired
    private GoodMainImgMapper goodMainImgMapper;

    /**
     * 查询商品主图片
     * 
     * @param id 商品主图片主键
     * @return 商品主图片
     */
    @Override
    public GoodMainImg selectGoodMainImgById(Long id)
    {
        return goodMainImgMapper.selectGoodMainImgById(id);
    }

    /**
     * 查询商品主图片列表
     * 
     * @param goodMainImg 商品主图片
     * @return 商品主图片
     */
    @Override
    public List<GoodMainImg> selectGoodMainImgList(GoodMainImg goodMainImg)
    {
        return goodMainImgMapper.selectGoodMainImgList(goodMainImg);
    }

    /**
     * 新增商品主图片
     * 
     * @param goodMainImg 商品主图片
     * @return 结果
     */
    @Override
    public int insertGoodMainImg(GoodMainImg goodMainImg)
    {
        return goodMainImgMapper.insertGoodMainImg(goodMainImg);
    }

    /**
     * 修改商品主图片
     * 
     * @param goodMainImg 商品主图片
     * @return 结果
     */
    @Override
    public int updateGoodMainImg(GoodMainImg goodMainImg)
    {
        return goodMainImgMapper.updateGoodMainImg(goodMainImg);
    }

    /**
     * 批量删除商品主图片
     * 
     * @param ids 需要删除的商品主图片主键
     * @return 结果
     */
    @Override
    public int deleteGoodMainImgByIds(Long[] ids)
    {
        return goodMainImgMapper.deleteGoodMainImgByIds(ids);
    }

    /**
     * 删除商品主图片信息
     * 
     * @param id 商品主图片主键
     * @return 结果
     */
    @Override
    public int deleteGoodMainImgById(Long id)
    {
        return goodMainImgMapper.deleteGoodMainImgById(id);
    }
}
