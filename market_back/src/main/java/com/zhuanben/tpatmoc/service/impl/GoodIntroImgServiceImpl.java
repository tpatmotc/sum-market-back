package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zhuanben.tpatmoc.mapper.GoodIntroImgMapper;
import com.zhuanben.tpatmoc.domain.GoodIntroImg;
import com.zhuanben.tpatmoc.service.IGoodIntroImgService;

/**
 * 商品介绍图片Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-11
 */
@Service
public class GoodIntroImgServiceImpl implements IGoodIntroImgService 
{
    @Autowired
    private GoodIntroImgMapper goodIntroImgMapper;

    /**
     * 查询商品介绍图片
     * 
     * @param id 商品介绍图片主键
     * @return 商品介绍图片
     */
    @Override
    public GoodIntroImg selectGoodIntroImgById(Long id)
    {
        return goodIntroImgMapper.selectGoodIntroImgById(id);
    }

    /**
     * 查询商品介绍图片列表
     * 
     * @param goodIntroImg 商品介绍图片
     * @return 商品介绍图片
     */
    @Override
    public List<GoodIntroImg> selectGoodIntroImgList(GoodIntroImg goodIntroImg)
    {
        return goodIntroImgMapper.selectGoodIntroImgList(goodIntroImg);
    }

    /**
     * 新增商品介绍图片
     * 
     * @param goodIntroImg 商品介绍图片
     * @return 结果
     */
    @Override
    public int insertGoodIntroImg(GoodIntroImg goodIntroImg)
    {
        return goodIntroImgMapper.insertGoodIntroImg(goodIntroImg);
    }

    /**
     * 修改商品介绍图片
     * 
     * @param goodIntroImg 商品介绍图片
     * @return 结果
     */
    @Override
    public int updateGoodIntroImg(GoodIntroImg goodIntroImg)
    {
        return goodIntroImgMapper.updateGoodIntroImg(goodIntroImg);
    }

    /**
     * 批量删除商品介绍图片
     * 
     * @param ids 需要删除的商品介绍图片主键
     * @return 结果
     */
    @Override
    public int deleteGoodIntroImgByIds(Long[] ids)
    {
        return goodIntroImgMapper.deleteGoodIntroImgByIds(ids);
    }

    /**
     * 删除商品介绍图片信息
     * 
     * @param id 商品介绍图片主键
     * @return 结果
     */
    @Override
    public int deleteGoodIntroImgById(Long id)
    {
        return goodIntroImgMapper.deleteGoodIntroImgById(id);
    }
}
