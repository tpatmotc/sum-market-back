package com.zhuanben.tpatmoc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zhuanben.tpatmoc.mapper.CommentImgMapper;
import com.zhuanben.tpatmoc.domain.CommentImg;
import com.zhuanben.tpatmoc.service.ICommentImgService;

/**
 * 评论图片Service业务层处理
 * 
 * @author ldp
 * @date 2022-11-10
 */
@Service
public class CommentImgServiceImpl implements ICommentImgService 
{
    @Autowired
    private CommentImgMapper commentImgMapper;

    /**
     * 查询评论图片
     * 
     * @param id 评论图片主键
     * @return 评论图片
     */
    @Override
    public CommentImg selectCommentImgById(Long id)
    {
        return commentImgMapper.selectCommentImgById(id);
    }

    /**
     * 查询评论图片列表
     * 
     * @param commentImg 评论图片
     * @return 评论图片
     */
    @Override
    public List<CommentImg> selectCommentImgList(CommentImg commentImg)
    {
        return commentImgMapper.selectCommentImgList(commentImg);
    }

    /**
     * 新增评论图片
     * 
     * @param commentImg 评论图片
     * @return 结果
     */
    @Override
    public int insertCommentImg(CommentImg commentImg)
    {
        return commentImgMapper.insertCommentImg(commentImg);
    }

    /**
     * 修改评论图片
     * 
     * @param commentImg 评论图片
     * @return 结果
     */
    @Override
    public int updateCommentImg(CommentImg commentImg)
    {
        return commentImgMapper.updateCommentImg(commentImg);
    }

    /**
     * 批量删除评论图片
     * 
     * @param ids 需要删除的评论图片主键
     * @return 结果
     */
    @Override
    public int deleteCommentImgByIds(Long[] ids)
    {
        return commentImgMapper.deleteCommentImgByIds(ids);
    }

    /**
     * 删除评论图片信息
     * 
     * @param id 评论图片主键
     * @return 结果
     */
    @Override
    public int deleteCommentImgById(Long id)
    {
        return commentImgMapper.deleteCommentImgById(id);
    }
}
