package com.zhuanben.tpatmoc.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.zhuanben.tpatmoc.domain.Admin;
import com.zhuanben.tpatmoc.service.IAdminService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 管理员Controller
 * 
 * @author ldp
 * @date 2022-11-11
 */
@RestController
@RequestMapping("/tpatmoc/sumAdmin")
public class AdminController extends BaseController
{
    @Autowired
    private IAdminService adminService;

    /**
     * 查询管理员列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:sumAdmin:list')")
    @GetMapping("/list")
    public TableDataInfo list(Admin admin)
    {
        startPage();
        List<Admin> list = adminService.selectAdminList(admin);
        return getDataTable(list);
    }

    /**
     * 导出管理员列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:sumAdmin:export')")
    @Log(title = "管理员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Admin admin)
    {
        List<Admin> list = adminService.selectAdminList(admin);
        ExcelUtil<Admin> util = new ExcelUtil<Admin>(Admin.class);
        util.exportExcel(response, list, "管理员数据");
    }

    /**
     * 获取管理员详细信息
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:sumAdmin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(adminService.selectAdminById(id));
    }

    /**
     * 新增管理员
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:sumAdmin:add')")
    @Log(title = "管理员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Admin admin)
    {
        return toAjax(adminService.insertAdmin(admin));
    }

    /**
     * 修改管理员
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:sumAdmin:edit')")
    @Log(title = "管理员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Admin admin)
    {
        return toAjax(adminService.updateAdmin(admin));
    }

    /**
     * 删除管理员
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:sumAdmin:remove')")
    @Log(title = "管理员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(adminService.deleteAdminByIds(ids));
    }
}
