package com.zhuanben.tpatmoc.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.zhuanben.tpatmoc.domain.GoodIntroImg;
import com.zhuanben.tpatmoc.service.IGoodIntroImgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品介绍图片Controller
 * 
 * @author ldp
 * @date 2022-11-11
 */
@RestController
@RequestMapping("/tpatmoc/goodsIntroImg")
public class GoodIntroImgController extends BaseController
{
    @Autowired
    private IGoodIntroImgService goodIntroImgService;

    /**
     * 查询商品介绍图片列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsIntroImg:list')")
    @GetMapping("/list")
    public TableDataInfo list(GoodIntroImg goodIntroImg)
    {
        startPage();
        List<GoodIntroImg> list = goodIntroImgService.selectGoodIntroImgList(goodIntroImg);
        return getDataTable(list);
    }

    /**
     * 导出商品介绍图片列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsIntroImg:export')")
    @Log(title = "商品介绍图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GoodIntroImg goodIntroImg)
    {
        List<GoodIntroImg> list = goodIntroImgService.selectGoodIntroImgList(goodIntroImg);
        ExcelUtil<GoodIntroImg> util = new ExcelUtil<GoodIntroImg>(GoodIntroImg.class);
        util.exportExcel(response, list, "商品介绍图片数据");
    }

    /**
     * 获取商品介绍图片详细信息
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsIntroImg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(goodIntroImgService.selectGoodIntroImgById(id));
    }

    /**
     * 新增商品介绍图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsIntroImg:add')")
    @Log(title = "商品介绍图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GoodIntroImg goodIntroImg)
    {
        return toAjax(goodIntroImgService.insertGoodIntroImg(goodIntroImg));
    }

    /**
     * 修改商品介绍图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsIntroImg:edit')")
    @Log(title = "商品介绍图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GoodIntroImg goodIntroImg)
    {
        return toAjax(goodIntroImgService.updateGoodIntroImg(goodIntroImg));
    }

    /**
     * 删除商品介绍图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsIntroImg:remove')")
    @Log(title = "商品介绍图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(goodIntroImgService.deleteGoodIntroImgByIds(ids));
    }
}
