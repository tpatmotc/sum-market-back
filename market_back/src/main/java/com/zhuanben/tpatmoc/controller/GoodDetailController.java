package com.zhuanben.tpatmoc.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.service.IGoodDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品详细Controller
 * 
 * @author ldp
 * @date 2022-11-11
 */
@RestController
@RequestMapping("/tpatmoc/goodsDetail")
public class GoodDetailController extends BaseController
{
    @Autowired
    private IGoodDetailService goodDetailService;

    /**
     * 查询商品详细列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsDetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(GoodDetail goodDetail)
    {
        startPage();
        List<GoodDetail> list = goodDetailService.selectGoodDetailList(goodDetail);
        return getDataTable(list);
    }

    /**
     * 导出商品详细列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsDetail:export')")
    @Log(title = "商品详细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GoodDetail goodDetail)
    {
        List<GoodDetail> list = goodDetailService.selectGoodDetailList(goodDetail);
        ExcelUtil<GoodDetail> util = new ExcelUtil<GoodDetail>(GoodDetail.class);
        util.exportExcel(response, list, "商品详细数据");
    }

    /**
     * 获取商品详细详细信息
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsDetail:query')")
    @GetMapping(value = "/{skuid}")
    public AjaxResult getInfo(@PathVariable("skuid") Long skuid)
    {
        return AjaxResult.success(goodDetailService.selectGoodDetailBySkuid(skuid));
    }

    /**
     * 新增商品详细
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsDetail:add')")
    @Log(title = "商品详细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GoodDetail goodDetail)
    {
        return toAjax(goodDetailService.insertGoodDetail(goodDetail));
    }

    /**
     * 修改商品详细
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsDetail:edit')")
    @Log(title = "商品详细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GoodDetail goodDetail)
    {
        return toAjax(goodDetailService.updateGoodDetail(goodDetail));
    }

    /**
     * 删除商品详细
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsDetail:remove')")
    @Log(title = "商品详细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{skuids}")
    public AjaxResult remove(@PathVariable Long[] skuids)
    {
        return toAjax(goodDetailService.deleteGoodDetailBySkuids(skuids));
    }
}
