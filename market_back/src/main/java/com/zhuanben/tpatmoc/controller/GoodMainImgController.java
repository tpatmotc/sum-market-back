package com.zhuanben.tpatmoc.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.zhuanben.tpatmoc.domain.GoodMainImg;
import com.zhuanben.tpatmoc.service.IGoodMainImgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品主图片Controller
 * 
 * @author ldp
 * @date 2022-11-11
 */
@RestController
@RequestMapping("/tpatmoc/goodsMainImg")
public class GoodMainImgController extends BaseController
{
    @Autowired
    private IGoodMainImgService goodMainImgService;

    /**
     * 查询商品主图片列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsMainImg:list')")
    @GetMapping("/list")
    public TableDataInfo list(GoodMainImg goodMainImg)
    {
        startPage();
        List<GoodMainImg> list = goodMainImgService.selectGoodMainImgList(goodMainImg);
        return getDataTable(list);
    }

    /**
     * 导出商品主图片列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsMainImg:export')")
    @Log(title = "商品主图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GoodMainImg goodMainImg)
    {
        List<GoodMainImg> list = goodMainImgService.selectGoodMainImgList(goodMainImg);
        ExcelUtil<GoodMainImg> util = new ExcelUtil<GoodMainImg>(GoodMainImg.class);
        util.exportExcel(response, list, "商品主图片数据");
    }

    /**
     * 获取商品主图片详细信息
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsMainImg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(goodMainImgService.selectGoodMainImgById(id));
    }

    /**
     * 新增商品主图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsMainImg:add')")
    @Log(title = "商品主图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GoodMainImg goodMainImg)
    {
        return toAjax(goodMainImgService.insertGoodMainImg(goodMainImg));
    }

    /**
     * 修改商品主图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsMainImg:edit')")
    @Log(title = "商品主图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GoodMainImg goodMainImg)
    {
        return toAjax(goodMainImgService.updateGoodMainImg(goodMainImg));
    }

    /**
     * 删除商品主图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:goodsMainImg:remove')")
    @Log(title = "商品主图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(goodMainImgService.deleteGoodMainImgByIds(ids));
    }
}
