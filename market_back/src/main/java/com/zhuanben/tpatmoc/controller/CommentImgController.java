package com.zhuanben.tpatmoc.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.zhuanben.tpatmoc.domain.CommentImg;
import com.zhuanben.tpatmoc.service.ICommentImgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 评论图片Controller
 * 
 * @author ldp
 * @date 2022-11-10
 */
@RestController
@RequestMapping("/tpatmoc/img")
public class CommentImgController extends BaseController
{
    @Autowired
    private ICommentImgService commentImgService;

    /**
     * 查询评论图片列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:img:list')")
    @GetMapping("/list")
    public TableDataInfo list(CommentImg commentImg)
    {
        startPage();
        List<CommentImg> list = commentImgService.selectCommentImgList(commentImg);
        return getDataTable(list);
    }

    /**
     * 导出评论图片列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:img:export')")
    @Log(title = "评论图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CommentImg commentImg)
    {
        List<CommentImg> list = commentImgService.selectCommentImgList(commentImg);
        ExcelUtil<CommentImg> util = new ExcelUtil<CommentImg>(CommentImg.class);
        util.exportExcel(response, list, "评论图片数据");
    }

    /**
     * 获取评论图片详细信息
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:img:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(commentImgService.selectCommentImgById(id));
    }

    /**
     * 新增评论图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:img:add')")
    @Log(title = "评论图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CommentImg commentImg)
    {
        return toAjax(commentImgService.insertCommentImg(commentImg));
    }

    /**
     * 修改评论图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:img:edit')")
    @Log(title = "评论图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CommentImg commentImg)
    {
        return toAjax(commentImgService.updateCommentImg(commentImg));
    }

    /**
     * 删除评论图片
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:img:remove')")
    @Log(title = "评论图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(commentImgService.deleteCommentImgByIds(ids));
    }
}
