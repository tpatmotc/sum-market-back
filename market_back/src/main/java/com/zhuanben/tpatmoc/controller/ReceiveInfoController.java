package com.zhuanben.tpatmoc.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.zhuanben.tpatmoc.domain.ReceiveInfo;
import com.zhuanben.tpatmoc.service.IReceiveInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 收件地址Controller
 * 
 * @author ldp
 * @date 2022-11-11
 */
@RestController
@RequestMapping("/tpatmoc/info")
public class ReceiveInfoController extends BaseController
{
    @Autowired
    private IReceiveInfoService receiveInfoService;

    /**
     * 查询收件地址列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(ReceiveInfo receiveInfo)
    {
        startPage();
        List<ReceiveInfo> list = receiveInfoService.selectReceiveInfoList(receiveInfo);
        return getDataTable(list);
    }

    /**
     * 导出收件地址列表
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:info:export')")
    @Log(title = "收件地址", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ReceiveInfo receiveInfo)
    {
        List<ReceiveInfo> list = receiveInfoService.selectReceiveInfoList(receiveInfo);
        ExcelUtil<ReceiveInfo> util = new ExcelUtil<ReceiveInfo>(ReceiveInfo.class);
        util.exportExcel(response, list, "收件地址数据");
    }

    /**
     * 获取收件地址详细信息
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(receiveInfoService.selectReceiveInfoById(id));
    }

    /**
     * 新增收件地址
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:info:add')")
    @Log(title = "收件地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ReceiveInfo receiveInfo)
    {
        return toAjax(receiveInfoService.insertReceiveInfo(receiveInfo));
    }

    /**
     * 修改收件地址
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:info:edit')")
    @Log(title = "收件地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ReceiveInfo receiveInfo)
    {
        return toAjax(receiveInfoService.updateReceiveInfo(receiveInfo));
    }

    /**
     * 删除收件地址
     */
    @PreAuthorize("@ss.hasPermi('tpatmoc:info:remove')")
    @Log(title = "收件地址", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(receiveInfoService.deleteReceiveInfoByIds(ids));
    }
}
