package com.zhuanben.tpatmoc.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品详细对象 t_good_detail
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class GoodDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 最小库存单位编码 */
    private Long skuid;

    /** 型号id */
    @Excel(name = "型号id")
    private Long modelId;

    /** 商品所属号 */
    @Excel(name = "商品所属号")
    private Long goodId;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 自定义的商品参数 */
    @Excel(name = "自定义的商品参数")
    private String info;

    /** 颜色 */
    @Excel(name = "颜色")
    private String color;

    /** 规格 */
    @Excel(name = "规格")
    private String storage;

    /** 库存数量 */
    @Excel(name = "库存数量")
    private Long stock;

    /** 状态 */
    @Excel(name = "状态")
    private Long state;

    public void setSkuid(Long skuid) 
    {
        this.skuid = skuid;
    }

    public Long getSkuid() 
    {
        return skuid;
    }
    public void setModelId(Long modelId) 
    {
        this.modelId = modelId;
    }

    public Long getModelId() 
    {
        return modelId;
    }
    public void setGoodId(Long goodId) 
    {
        this.goodId = goodId;
    }

    public Long getGoodId() 
    {
        return goodId;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setInfo(String info) 
    {
        this.info = info;
    }

    public String getInfo() 
    {
        return info;
    }
    public void setColor(String color) 
    {
        this.color = color;
    }

    public String getColor() 
    {
        return color;
    }
    public void setStorage(String storage) 
    {
        this.storage = storage;
    }

    public String getStorage() 
    {
        return storage;
    }
    public void setStock(Long stock) 
    {
        this.stock = stock;
    }

    public Long getStock() 
    {
        return stock;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("skuid", getSkuid())
            .append("modelId", getModelId())
            .append("goodId", getGoodId())
            .append("price", getPrice())
            .append("info", getInfo())
            .append("color", getColor())
            .append("storage", getStorage())
            .append("stock", getStock())
            .append("state", getState())
            .toString();
    }
}
