package com.zhuanben.tpatmoc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 角色对象 t_role
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class Role extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 角色id */
    private Long id;

    /** 角色名 */
    @Excel(name = "角色名")
    private String roleName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRoleName(String roleName) 
    {
        this.roleName = roleName;
    }

    public String getRoleName() 
    {
        return roleName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("roleName", getRoleName())
            .toString();
    }
}
