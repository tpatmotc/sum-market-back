package com.zhuanben.tpatmoc.domain;

import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品对象 t_good
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class Goods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商品id */
    private Long id;

    /** 商品标题 */
    @Excel(name = "商品标题")
    private String title;

    /** 一级分类 */
    @Excel(name = "一级分类")
    private Long categoryLevelOne;

    /** 二级分类 */
    @Excel(name = "二级分类")
    private Long categoryLevelTwo;

    /** 三级分类 */
    @Excel(name = "三级分类")
    private Long categoryLevelThree;

    /** 商品状态 （0删除 1上架 2下架） */
    @Excel(name = "商品状态 ", readConverterExp = "0=删除,1=上架,2=下架")
    private String state;

    /** 最低价 */
    @Excel(name = "最低价")
    private BigDecimal lowestPrice;

    /** 商品详细信息 */
    private List<GoodDetail> goodDetailList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setCategoryLevelOne(Long categoryLevelOne) 
    {
        this.categoryLevelOne = categoryLevelOne;
    }

    public Long getCategoryLevelOne() 
    {
        return categoryLevelOne;
    }
    public void setCategoryLevelTwo(Long categoryLevelTwo) 
    {
        this.categoryLevelTwo = categoryLevelTwo;
    }

    public Long getCategoryLevelTwo() 
    {
        return categoryLevelTwo;
    }
    public void setCategoryLevelThree(Long categoryLevelThree) 
    {
        this.categoryLevelThree = categoryLevelThree;
    }

    public Long getCategoryLevelThree() 
    {
        return categoryLevelThree;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setLowestPrice(BigDecimal lowestPrice) 
    {
        this.lowestPrice = lowestPrice;
    }

    public BigDecimal getLowestPrice() 
    {
        return lowestPrice;
    }

    public List<GoodDetail> getGoodDetailList()
    {
        return goodDetailList;
    }

    public void setGoodDetailList(List<GoodDetail> goodDetailList)
    {
        this.goodDetailList = goodDetailList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("categoryLevelOne", getCategoryLevelOne())
            .append("categoryLevelTwo", getCategoryLevelTwo())
            .append("categoryLevelThree", getCategoryLevelThree())
            .append("state", getState())
            .append("lowestPrice", getLowestPrice())
            .append("goodDetailList", getGoodDetailList())
            .toString();
    }
}
