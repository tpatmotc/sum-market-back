package com.zhuanben.tpatmoc.domain;

import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单对象 t_order
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class Order extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long uid;

    /** 收件id */
    @Excel(name = "收件id")
    private Long receiveId;

    /** 实付金额 */
    @Excel(name = "实付金额")
    private BigDecimal payment;

    /** 订单付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单付款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 订单结算时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单结算时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date finishTime;

    /** 订单状态 1未付款，2已付款，3已失效，4已完成 */
    @Excel(name = "订单状态 1未付款，2已付款，3已失效，4已完成")
    private String state;

    /** 订单详情信息 */
    private List<OrderDetail> orderDetailList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUid(Long uid) 
    {
        this.uid = uid;
    }

    public Long getUid() 
    {
        return uid;
    }
    public void setReceiveId(Long receiveId) 
    {
        this.receiveId = receiveId;
    }

    public Long getReceiveId() 
    {
        return receiveId;
    }
    public void setPayment(BigDecimal payment) 
    {
        this.payment = payment;
    }

    public BigDecimal getPayment() 
    {
        return payment;
    }
    public void setPayTime(Date payTime) 
    {
        this.payTime = payTime;
    }

    public Date getPayTime() 
    {
        return payTime;
    }
    public void setFinishTime(Date finishTime) 
    {
        this.finishTime = finishTime;
    }

    public Date getFinishTime() 
    {
        return finishTime;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }

    public List<OrderDetail> getOrderDetailList()
    {
        return orderDetailList;
    }

    public void setOrderDetailList(List<OrderDetail> orderDetailList)
    {
        this.orderDetailList = orderDetailList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("uid", getUid())
            .append("receiveId", getReceiveId())
            .append("payment", getPayment())
            .append("createTime", getCreateTime())
            .append("payTime", getPayTime())
            .append("finishTime", getFinishTime())
            .append("state", getState())
            .append("orderDetailList", getOrderDetailList())
            .toString();
    }
}
