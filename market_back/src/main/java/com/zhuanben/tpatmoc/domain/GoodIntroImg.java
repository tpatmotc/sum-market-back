package com.zhuanben.tpatmoc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品介绍图片对象 t_good_intro_img
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class GoodIntroImg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 详情图id */
    private Long id;

    /** 商品id */
    @Excel(name = "商品id")
    private Long gid;

    /** 图片路径 */
    @Excel(name = "图片路径")
    private String imgPath;

    /** 状态 */
    @Excel(name = "状态")
    private String state;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGid(Long gid) 
    {
        this.gid = gid;
    }

    public Long getGid() 
    {
        return gid;
    }
    public void setImgPath(String imgPath) 
    {
        this.imgPath = imgPath;
    }

    public String getImgPath() 
    {
        return imgPath;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gid", getGid())
            .append("imgPath", getImgPath())
            .append("state", getState())
            .toString();
    }
}
