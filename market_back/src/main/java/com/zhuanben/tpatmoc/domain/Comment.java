package com.zhuanben.tpatmoc.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户评论对象 t_comment
 * 
 * @author ldp
 * @date 2022-11-10
 */
public class Comment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评论id */
    private Long id;

    /** 评论等级 （1一星 2二星 3三星 4四星 5五星） */
    @Excel(name = "评论等级 ", readConverterExp = "1=一星,2=二星,3=三星,4=四星,5=五星")
    private String commLevel;

    /** 用户id */
    @Excel(name = "用户id")
    private Long uid;

    /** 商品的skuid */
    @Excel(name = "商品的skuid")
    private Long skuid;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderDetailId;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 发表时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发表时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 状态 */
    @Excel(name = "状态")
    private String state;

    /** 评论图片信息 */
    private List<CommentImg> commentImgList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCommLevel(String commLevel) 
    {
        this.commLevel = commLevel;
    }

    public String getCommLevel() 
    {
        return commLevel;
    }
    public void setUid(Long uid) 
    {
        this.uid = uid;
    }

    public Long getUid() 
    {
        return uid;
    }
    public void setSkuid(Long skuid) 
    {
        this.skuid = skuid;
    }

    public Long getSkuid() 
    {
        return skuid;
    }
    public void setOrderDetailId(Long orderDetailId) 
    {
        this.orderDetailId = orderDetailId;
    }

    public Long getOrderDetailId() 
    {
        return orderDetailId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }

    public List<CommentImg> getCommentImgList()
    {
        return commentImgList;
    }

    public void setCommentImgList(List<CommentImg> commentImgList)
    {
        this.commentImgList = commentImgList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("commLevel", getCommLevel())
            .append("uid", getUid())
            .append("skuid", getSkuid())
            .append("orderDetailId", getOrderDetailId())
            .append("content", getContent())
            .append("publishTime", getPublishTime())
            .append("state", getState())
            .append("commentImgList", getCommentImgList())
            .toString();
    }
}
