package com.zhuanben.tpatmoc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户对象 t_user
 * 
 * @author ldp
 * @date 2022-12-24
 */
public class User extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    private Long id;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 加密后的字符 */
    @Excel(name = "加密后的字符")
    private String password;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    /** 头像url */
    @Excel(name = "头像url")
    private String avatar;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String nickname;

    /** 微信id */
    @Excel(name = "微信id")
    private String openId;

    /** 星豆 */
    @Excel(name = "星豆")
    private Long starPoint;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setAvatar(String avatar) 
    {
        this.avatar = avatar;
    }

    public String getAvatar() 
    {
        return avatar;
    }
    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getNickname() 
    {
        return nickname;
    }
    public void setOpenId(String openId) 
    {
        this.openId = openId;
    }

    public String getOpenId() 
    {
        return openId;
    }
    public void setStarPoint(Long starPoint) 
    {
        this.starPoint = starPoint;
    }

    public Long getStarPoint() 
    {
        return starPoint;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("tel", getTel())
            .append("avatar", getAvatar())
            .append("nickname", getNickname())
            .append("openId", getOpenId())
            .append("starPoint", getStarPoint())
            .toString();
    }
}
