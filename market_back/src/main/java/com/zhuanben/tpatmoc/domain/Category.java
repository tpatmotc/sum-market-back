package com.zhuanben.tpatmoc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品分类对象 t_category
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class Category extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分类id */
    private Long id;

    /** 分类等级 */
    @Excel(name = "分类等级")
    private Long categoryLevel;

    /** 分类类型 */
    @Excel(name = "分类类型")
    private String categoryType;

    /** 父分类id */
    @Excel(name = "父分类id")
    private Long parentId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCategoryLevel(Long categoryLevel) 
    {
        this.categoryLevel = categoryLevel;
    }

    public Long getCategoryLevel() 
    {
        return categoryLevel;
    }
    public void setCategoryType(String categoryType) 
    {
        this.categoryType = categoryType;
    }

    public String getCategoryType() 
    {
        return categoryType;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("categoryLevel", getCategoryLevel())
            .append("categoryType", getCategoryType())
            .append("parentId", getParentId())
            .toString();
    }
}
