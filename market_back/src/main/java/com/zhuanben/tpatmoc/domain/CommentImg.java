package com.zhuanben.tpatmoc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 评论图片对象 t_comment_img
 * 
 * @author ldp
 * @date 2022-11-10
 */
public class CommentImg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评论图片id */
    private Long id;

    /** 评论id */
    @Excel(name = "评论id")
    private Long commId;

    /** 评论图片地址 */
    @Excel(name = "评论图片地址")
    private String imgPath;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCommId(Long commId) 
    {
        this.commId = commId;
    }

    public Long getCommId() 
    {
        return commId;
    }
    public void setImgPath(String imgPath) 
    {
        this.imgPath = imgPath;
    }

    public String getImgPath() 
    {
        return imgPath;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("commId", getCommId())
            .append("imgPath", getImgPath())
            .toString();
    }
}
