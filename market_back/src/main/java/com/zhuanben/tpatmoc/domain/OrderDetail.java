package com.zhuanben.tpatmoc.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单详情对象 t_order_detail
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class OrderDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单id */
    private Long id;

    /** 所属父订单id */
    @Excel(name = "所属父订单id")
    private Long oid;

    /** 最小库存单位编码 */
    @Excel(name = "最小库存单位编码")
    private Long skuid;

    /** 数量 */
    @Excel(name = "数量")
    private Long amount;

    /** 实付价格 */
    @Excel(name = "实付价格")
    private BigDecimal unitPayment;

    /** 订单状态 （1未付款 2已付款待发货 3取消订单 4交易完成 5交易失败） */
    @Excel(name = "订单状态 ", readConverterExp = "1=未付款,2=已付款待发货,3=取消订单,4=交易完成,5=交易失败")
    private String state;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOid(Long oid) 
    {
        this.oid = oid;
    }

    public Long getOid() 
    {
        return oid;
    }
    public void setSkuid(Long skuid) 
    {
        this.skuid = skuid;
    }

    public Long getSkuid() 
    {
        return skuid;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setUnitPayment(BigDecimal unitPayment) 
    {
        this.unitPayment = unitPayment;
    }

    public BigDecimal getUnitPayment() 
    {
        return unitPayment;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("oid", getOid())
            .append("skuid", getSkuid())
            .append("amount", getAmount())
            .append("unitPayment", getUnitPayment())
            .append("state", getState())
            .toString();
    }
}
