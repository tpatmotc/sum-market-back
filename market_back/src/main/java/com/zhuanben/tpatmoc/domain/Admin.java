package com.zhuanben.tpatmoc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 管理员对象 t_admin
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class Admin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 管理员id */
    private Long id;

    /** 管理员账号 */
    @Excel(name = "管理员账号")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("tel", getTel())
            .toString();
    }
}
