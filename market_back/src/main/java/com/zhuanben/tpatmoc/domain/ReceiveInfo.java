package com.zhuanben.tpatmoc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收件地址对象 t_receive_info
 * 
 * @author ldp
 * @date 2022-11-11
 */
public class ReceiveInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 收件人id */
    private Long id;

    /** 收件人地址 */
    @Excel(name = "收件人地址")
    private String receiveAddress;

    /** 收件人姓名 */
    @Excel(name = "收件人姓名")
    private String receiveName;

    /** 收件人电话 */
    @Excel(name = "收件人电话")
    private String receiveTel;

    /** 收件信息所属用户（谁创造的该收件信息） */
    @Excel(name = "收件信息所属用户", readConverterExp = "谁=创造的该收件信息")
    private Long uid;

    /** 收件状态 （0正常 1停用） */
    @Excel(name = "收件状态 ", readConverterExp = "0=正常,1=停用")
    private String state;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setReceiveAddress(String receiveAddress) 
    {
        this.receiveAddress = receiveAddress;
    }

    public String getReceiveAddress() 
    {
        return receiveAddress;
    }
    public void setReceiveName(String receiveName) 
    {
        this.receiveName = receiveName;
    }

    public String getReceiveName() 
    {
        return receiveName;
    }
    public void setReceiveTel(String receiveTel) 
    {
        this.receiveTel = receiveTel;
    }

    public String getReceiveTel() 
    {
        return receiveTel;
    }
    public void setUid(Long uid) 
    {
        this.uid = uid;
    }

    public Long getUid() 
    {
        return uid;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("receiveAddress", getReceiveAddress())
            .append("receiveName", getReceiveName())
            .append("receiveTel", getReceiveTel())
            .append("uid", getUid())
            .append("state", getState())
            .toString();
    }
}
