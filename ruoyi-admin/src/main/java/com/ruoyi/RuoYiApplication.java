package com.ruoyi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * 启动程序
 * 
 * @author ruoyi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@MapperScan(value = {"com.zhuanben.*.mapper","com.ruoyi.*.*.mapper"})
@ComponentScan(value = {"com.zhuanben","com.ruoyi"} )
public class RuoYiApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(RuoYiApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  人民当家作组三星商城后台管理系统启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "       #           #         #           #                ##            #    #           #            \n" +
                "       ###         ############     #    ##   #       #    ##    #      ##   ##          ## #     #   \n" +
                "       ##          ##       ##       ##  ##  ###     ##############     ##  ##    #     ##  ########  \n" +
                "       ##          ##       ##       ### ##  ##     ###         ##     ##   ########   ##   ##   ##   \n" +
                "       ##          ##       ##        #  ## ##       #  ####### #      #   ####        #  # ##   ##   \n" +
                "       ##          ###########           ###   #         ###          ###  # ##  #    #  #####   ##   \n" +
                "      ## #         ##   ##  #       #############       ###    #      ### #  ######   ##### #######   \n" +
                "      ## #         ##   ##    #               ##       ## ##  ###    # ##    ##         ##  ##   ##   \n" +
                "      ## ##        #############              ##     ##  ######        ##    ##        ##   ##   ##   \n" +
                "     ##   #        ##    ##                   ##        ## ###         ##    ##   #   ##### ##   ##   \n" +
                "     ##   ##       ##    ##          ###########       ## #####        ##    #######   #    #######   \n" +
                "    ##     ##      ##     ##                  ##     ##  #### ##       ##    ##            ###   ##   \n" +
                "   ##      ###     ##   # ##   #              ##        ## ##  ###     ##    ##          ## ##   ##   \n" +
                "   #        ###    #####   ##  #              ##      ##   ##   ###    ##    ##       ####  ##   ##   \n" +
                "  #          ###  ####      #####   ############    ##   ####    #     ##    ##        #    ##   ##   \n" +
                " #            #    #          ##              #           ##           #     ##           ########### \n" +
                "\n");
    }
}
